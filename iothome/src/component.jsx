import React, { Component } from "react";
import { AwesomeButton } from "react-awesome-button";
import "react-awesome-button/dist/styles.css";
import "react-awesome-button/dist/themes/theme-eric.css";
//import styles from "./component.css"
import openSocket from "socket.io-client";
const socket = openSocket();
//const socket = openSocket("https://iotsergiohome.mybluemix.net");
//const socket = openSocket("http://104.248.237.151:4000/");

export default class ButtonAwesomeLight extends Component {
  constructor(props) {
    super(props);
    // Don't call this.setState() here!
    this.state = { adc: 0, connected: false, latency: null };
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick() {
    const { adc } = this.state;
    const val = adc === 0 ? 1 : 0;
    //.setState({ adc: val });
    socket.emit("this", { led: val });
  }
  componentWillMount() {
    socket.on("connect", () => {
      this.setState({ connected: socket.connected });
      console.log("conectado", true); // true
      socket.on("pong", latency => {
        this.setState({ latency });
        //        console.log(latency);
      });
      socket.on("disconnect", reason => {
        console.log(reason);
        this.setState({ connected: false });
      });
    });
    socket.on("news", data => {
      this.setState({ adc: data.adc });
      console.log(data);
    });
  }

  render() {
    const { adc, connected } = this.state;
    return (
      <AwesomeButton
        //    cssModule={styles}
        type={adc === 0 ? "secondary" : "primary"}
        disabled={!connected}
        size="large"
        ripple
        onPress={() => this.handleClick()}
      >
        {adc === 0 ? "Encender" : "Apagar"}
      </AwesomeButton>
    );
  }
}
