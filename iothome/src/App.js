import React from "react";
import "./App.css";
import ButtonAwesomeLight from "./component";
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <ButtonAwesomeLight />
      </header>
    </div>
  );
}

export default App;
